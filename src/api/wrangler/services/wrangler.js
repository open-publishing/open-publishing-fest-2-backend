'use strict';

/**
 * wrangler service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::wrangler.wrangler');
