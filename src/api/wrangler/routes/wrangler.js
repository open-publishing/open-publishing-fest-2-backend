'use strict';

/**
 * wrangler router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::wrangler.wrangler');
