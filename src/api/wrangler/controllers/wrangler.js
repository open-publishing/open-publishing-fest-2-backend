'use strict';

/**
 * wrangler controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::wrangler.wrangler');
