'use strict';

/**
 * event-update router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::event-update.event-update');
