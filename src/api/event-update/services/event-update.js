'use strict';

/**
 * event-update service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::event-update.event-update');
