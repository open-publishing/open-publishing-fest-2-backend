'use strict';

/**
 * event-update controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::event-update.event-update');
